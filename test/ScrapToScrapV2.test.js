const { expect, assert } = require('chai');
const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

const { getTests, getMintableTests, getBurnableTests, getPausableTests, getAccessControlTests } = require("../utils/tests/ERC20");

require('dotenv').config();

const Scrap = artifacts.require('Scrap');
const ScrapV2 = artifacts.require('ScrapV2');

const BN0 = new BN("0");
const BN1 = new BN('1');
const BN10 = new BN('10');
const BN20 = new BN('20');

contract('Scrap To ScrapV2 Interaction', async (accounts) => {
  
  before(async function () {
    this.scrapOld = await Scrap.deployed(); 
    this.scrapNew = await ScrapV2.deployed(); 
  });

  it('can execute withdrawOrderPayment even if balance and allowance are on old scrap', async function() {
    const [ admin, user1, user2 ] = accounts;

    await this.scrapOld.mint(user2, BN20, { from: admin }); //mint old
    await this.scrapOld.approve(admin, BN10, { from: user2 }); //admin has 10 allowance

    const adminAllowance = await this.scrapNew.allowance(user2, admin);
    const adminBalance = await this.scrapNew.balanceOf(admin);

    expect(adminAllowance.eq(BN10)); //10

    await this.scrapNew.withdrawOrderPayment(user2, web3.utils.sha3((new Date()).getTime().toString()), BN10, { from: admin });

    const adminBalance2 = await this.scrapNew.balanceOf(admin);
    expect(adminBalance2.eq(adminBalance.add(BN10)));
  });

  it('carries balance from old Scrap is to new ScrapV2', async function() {
    const [ admin, user1, user2 ] = accounts;

    const scrapOldBal = await this.scrapNew.balanceOf(user1);

    await this.scrapOld.mint(user1, BN1, { from: admin });

    const scrapNewBal = await this.scrapNew.balanceOf(user1);

    expect(scrapNewBal.eq(scrapOldBal.add(BN1)));
  });

  it('should not update balance on the old Scrap when minting with new Scrap', async function() {
    const [ admin, user1, user2 ] = accounts;

    this.scrapOld.mint(user1, BN1, { from: admin });
    const scrapOldBal = await this.scrapNew.balanceOf(user1);

    //console.log(`user2 ${user2} balance from old scrap is ${scrapOldBal.toString()}`);
    //console.log(`admin ${admin} mints ${BN1.toString()} to user1 ${user1}`);
    //console.log(`admin has minter role ${await this.scrapNew.hasRole( web3.utils.keccak256("MINTER_ROLE"), admin )}`);
    
    await this.scrapNew.mint(user1, BN1, { from: admin });

    const scrapLatestOldBal = await this.scrapOld.balanceOf(user1);
    const scrapLatestNewBal = await this.scrapNew.balanceOf(user1);

    expect(scrapOldBal.eq(scrapLatestOldBal));
    expect(scrapLatestNewBal.eq(scrapOldBal.add(BN1)));
  });

  it('should carry out new balances from old Scrap to new Scrap when we by any means (accidentally / deliberately), minted on old Scrap', async function() {
    const [ admin, user1, user2 ] = accounts;

    const scrapNewBal1 = await this.scrapNew.balanceOf(user2);
    await this.scrapNew.mint(user2, BN10, { from: admin });
    const scrapNewBal2 = await this.scrapNew.balanceOf(user2);

    expect(scrapNewBal2.eq(scrapNewBal1.add(BN10)));

    //mint using ScrapOld
    this.scrapOld.mint(user2, BN1, { from: admin });
    const scrapNewBal3 = await this.scrapNew.balanceOf(user2);

    //ScrapNew should show an incremented balance
    expect(scrapNewBal3.eq(scrapNewBal2.add(BN1)));

    //mint using ScrapOld
    this.scrapOld.mint(user2, BN10, { from: admin });
    const scrapNewBal4 = await this.scrapNew.balanceOf(user2);

    //ScrapNew should show an incremented balance
    expect(scrapNewBal4.eq(scrapNewBal3.add(BN10)));

    //burn 1 token from user2
    //await this.scrapNew.burn(BN1, { from: user2 });

    //expect((await this.scrapNew.balanceOf(user2)).eq(scrapNewBal2));
  });

  it('should carry out new allowances from old Scrap to new Scrap when we by any means (accidentally / deliberately), approved on old Scrap', async function() {
    const [ admin, user1, user2 ] = accounts;

    await this.scrapNew.mint(user2, BN10, { from: admin });

    const scrapNewAllowance0 = await this.scrapNew.allowance(user2, admin);

    //pre approved admin for 1 token of user2 under old scrap
    await this.scrapOld.approve(admin, BN1, { from: user2 });

    const scrapNewAllowance1 = await this.scrapNew.allowance(user2, admin);

    expect(scrapNewAllowance1.eq(scrapNewAllowance0.add(BN1)));

    //increasing allowance on new Scrap should add previous allowances
    await this.scrapNew.increaseAllowance(admin, BN1, { from: user2 });
    const scrapNewAllowance2 = await this.scrapNew.allowance(user2, admin);

    expect(scrapNewAllowance2.eq(scrapNewAllowance1.add(BN1)));

    //test if approval works
    const scrapNewBal1 = await this.scrapNew.balanceOf(user2);
    await this.scrapNew.burnFrom(user2, BN1, { from: admin });

    const scrapNewBal2 = await this.scrapNew.balanceOf(user2);
    expect(scrapNewBal2.eq(scrapNewBal1.sub(BN1)));
  });

  it('reflects totalSupply correctly between old and new scrap contract when there are tokens minted on both', async function() {
    const [ admin, user1, user2, user3 ] = accounts;

    const scrapOldSupply1 = await this.scrapOld.totalSupply();
    await this.scrapOld.mint(admin, BN10, { from: admin });
    const scrapOldSupply2 = await this.scrapOld.totalSupply();

    const scrapNewSupply1 = await this.scrapNew.totalSupply();

    expect(scrapOldSupply2.eq(scrapOldSupply1.add(BN10)));
    expect(scrapOldSupply2.eq(scrapOldSupply1.add(BN10)));
    expect(scrapNewSupply1.eq(scrapOldSupply1.add(BN10)));

    //mint using new contract
    await this.scrapNew.mint(admin, BN1, { from: admin });

    const scrapNewSupply2 = await this.scrapNew.totalSupply();

    expect(scrapNewSupply2.eq(scrapNewSupply1.add(BN1)));
  });

  it('reflects totalSupply correctly between old and new scrap contract when there are tokens burned on both', async function() {
    const [ admin, user1, user2, user3 ] = accounts;

    const scrapOldSupply1 = await this.scrapOld.totalSupply();
    const scrapNewSupply1 = await this.scrapNew.totalSupply();

    await this.scrapOld.mint(admin, BN10, { from: admin }); //mint 1
    await this.scrapOld.mint(admin, BN10, { from: admin }); //mint 2

    await this.scrapOld.burn(BN10, { from: admin }); //burn 1

    const scrapNewSupply2 = await this.scrapNew.totalSupply();

    expect(scrapNewSupply2.eq(scrapNewSupply1.add(BN10))); //burn 2

    //burn again, but using new contract
    await this.scrapNew.burn(BN10, { from: admin });

    //new contract should return its original value between burns
    expect(scrapNewSupply2.eq(scrapNewSupply1));
  });

  it('approval allowances if allowance are spent on new and user maged to spend another allowance from old', async function() {
    const [ admin, user1, user2 ] = accounts;

    await this.scrapOld.mint(user2, BN20, { from: admin }); //mint 1
    await this.scrapNew.mint(user2, BN20, { from: admin }); //mint 2

    await this.scrapOld.approve(admin, BN20, { from: user2 }); //mint 1
    await this.scrapNew.approve(admin, BN10, { from: user2 }); //mint 2

    const adminAllowance0 = await this.scrapNew.allowance(user2, admin); //10

    let user2Bal = await this.scrapNew.balanceOf(user2);
    
    expect(adminAllowance0.eq(BN10)); //10
    expect(user2Bal.eq(BN20.add(BN20))); //40

    await this.scrapNew.burnFrom(user2, BN10, { from: admin });

    user2Bal = await this.scrapNew.balanceOf(user2);
    expect(user2Bal.eq(BN20.add(BN10))); //30

    const adminAllowance1 = await this.scrapNew.allowance(user2, admin);
    expect(adminAllowance0.eq(BN0)); //0
  });

  it('increases of allowances will read from old scrap if no pre-approve allowance in new scrap contract', async function() {
    const [ admin, user1, user2 ] = accounts;

    await this.scrapOld.mint(user2, BN20, { from: admin }); //mint old
    await this.scrapOld.approve(admin, BN20, { from: user2 }); //approve old

    await this.scrapNew.increaseAllowance(admin, BN20, { from: user2 });
    const adminAllowance1 = await this.scrapNew.allowance(user2, admin);

    expect(adminAllowance1.eq(BN20.add(BN20))); //40
    
    await this.scrapNew.approve(admin, BN20, { from: user2 });
    const adminAllowance2 = await this.scrapNew.allowance(user2, admin);

    expect(adminAllowance2.eq(BN20)); //20
  });

  it('decreases of allowances will read from old scrap if no pre-approve allowance in new scrap contract', async function() {
    const [ admin, user1, user2 ] = accounts;

    await this.scrapOld.mint(user2, BN20, { from: admin }); //mint old
    await this.scrapOld.approve(admin, BN20, { from: user2 }); //approve old

    await this.scrapNew.decreaseAllowance(admin, BN10, { from: user2 });
    const adminAllowance1 = await this.scrapNew.allowance(user2, admin);

    expect(adminAllowance1.eq(BN10)); //10
    
    await this.scrapNew.approve(admin, BN20, { from: user2 });
    const adminAllowance2 = await this.scrapNew.allowance(user2, admin);

    expect(adminAllowance2.eq(BN20)); //20
  });

  it('decreases of allowances after new scrap approval should be correct', async function() {
    const [ admin, user1, user2 ] = accounts;

    await this.scrapOld.mint(user2, BN20, { from: admin }); //mint old
    await this.scrapOld.approve(admin, BN10, { from: user2 }); //approve old

    const adminAllowance1 = await this.scrapNew.allowance(user2, admin);
    await this.scrapNew.approve(admin, BN20, { from: user2 }); //APROVE NEW
    const adminAllowance2 = await this.scrapNew.allowance(user2, admin);

    expect(adminAllowance2.eq(BN20)); //20

    await this.scrapNew.decreaseAllowance(admin, BN10, { from: user2 });
    const adminAllowance3 = await this.scrapNew.allowance(user2, admin);
    expect(adminAllowance3.eq(BN10)); //10
  });

  it('increases of allowances after new scrap approval should be correct', async function() {
    const [ admin, user1, user2 ] = accounts;

    await this.scrapOld.mint(user2, BN20, { from: admin }); //mint old
    await this.scrapOld.approve(admin, BN10, { from: user2 }); //approve old

    const adminAllowance1 = await this.scrapNew.allowance(user2, admin);
    await this.scrapNew.approve(admin, BN20, { from: user2 }); //APROVE NEW
    const adminAllowance2 = await this.scrapNew.allowance(user2, admin);

    expect(adminAllowance2.eq(BN20)); //20

    await this.scrapNew.increaseAllowance(admin, BN10, { from: user2 });
    const adminAllowance3 = await this.scrapNew.allowance(user2, admin);
    expect(adminAllowance3.eq(BN20.add(BN10))); //30
  });

  // scrap old needs to be pausable , the test scenarios when old scrap is paused

}); 