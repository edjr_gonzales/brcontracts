const ethers = require("ethers");
const sigUtil = require('eth-sig-util');

const { expect, assert } = require('chai');
const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');
const { web3 } = require('@openzeppelin/test-helpers/src/setup');
const { getSignatureParameters } = require("../utils/tests/MetaTx");

require('dotenv').config();

const MNEMONIC = process.env.MNEMONIC;

const BN1 = new BN('1');
const BN10 = new BN('10');
const BN100 = new BN('100');

const Scrap = artifacts.require('Scrap');
const BRPart = artifacts.require('BRPart');
const PlaycheckGateway = artifacts.require('PlaycheckGateway');

contract('PlaycheckGateway', async (accounts) => {
  //console.log("accounts", accounts);

  const [ owner, user1, user2 ] = accounts;

  const domainType = [
    { name: "name", type: "string" },
    { name: "version", type: "string" },
    { name: "verifyingContract", type: "address" },
    { name: "salt", type: "bytes32" }
  ];
  
  const metaTransactionType = [
    { name: "nonce", type: "uint256" },
    { name: "from", type: "address" },
    { name: "functionSignature", type: "bytes" }
  ];

  before(async function () {
    this.scrap = await Scrap.deployed({ from: owner });
    this.part = await BRPart.deployed({ from: owner }); 
    this.portal = await PlaycheckGateway.deployed({ from: owner });
  });

  beforeEach(async function () {
    
  });

  it('can accept and posses ERC20 token via deposit interface', async function() {
    await this.scrap.mint(owner, BN100, { from: owner });

    const origPortalBalance = await this.scrap.balanceOf(this.portal.address);
    const origOwnerBalance = await this.scrap.balanceOf(owner);

    //approve portal for amount of tokens to be deposited
    const app = await this.scrap.approve(this.portal.address, BN100, { from: owner });
    
    const allowance = await this.scrap.allowance(owner, this.portal.address);
    //console.log("portal allowance", allowance);
    //console.log("portal", this.portal.address);
    //console.log("sender", owner);

    //send erc20 to portal
    const erc20Deposit = await this.portal.deposit(this.scrap.address, BN100);
    //console.log("erc20Deposit", erc20Deposit);

    expectEvent(erc20Deposit, 'ERC20Deposit', { token: this.scrap.address, source: owner, tokenAmt: BN100, sourceBal: origOwnerBalance.sub(BN100) });

    const portalBalance = await this.scrap.balanceOf(this.portal.address);
    const ownerBalance = await this.scrap.balanceOf(owner);

    //console.log("origPortalBalance", origPortalBalance);
    //console.log("portalBalance", portalBalance);
    //console.log("origOwnerBalance", origOwnerBalance);
    //console.log("ownerBalance", ownerBalance);

    //portal has balance remains but sender balance deducted
    expect(portalBalance.eq(origPortalBalance) && ownerBalance.eq(origOwnerBalance.sub(BN100))).to.be.true;
  });

  it('can increase token user nonce on ERC20 deposit', async function() {
    await this.scrap.mint(owner, BN100, { from: owner });

    const origTokenUserNonce = await this.portal.tokenUserNonce(this.scrap.address, owner);

    //approve portal for amoutn fo tokens to be deposited
    const b = await this.scrap.balanceOf(owner);
    const t = await this.scrap.approve(this.portal.address, BN100, { from: owner });
    const allowance = await this.scrap.allowance(owner, this.portal.address);


    //send erc20 to portal
    await this.portal.deposit(this.scrap.address, BN100);

    const newTokenUserNonce = await this.portal.tokenUserNonce(this.scrap.address, owner);

    expect(origTokenUserNonce.lt(newTokenUserNonce)).to.be.true;
  });

  it('calling withdraw for ERC20 on bridge triggers promise rejection', async function() {
    const origPortalBalance = await this.scrap.balanceOf(this.portal.address);
    const origOwnerBalance = await this.scrap.balanceOf(owner);

    //send erc20 to owner

    try {
      await this.portal.withdraw(this.scrap.address, BN1, owner);
      expect.fail("No error happened");
    } catch (err){
      expect(err.toString().indexOf("No ERC20 Withdraw")).to.be.above(-1);
    }
  });

  /* it('allow withdraw and emits Withdraw event', async function() {
    const erc20Withdraw = await this.portal.withdraw(this.scrap.address, BN1, owner);
    
    //portal has balance updated
    expectEvent(erc20Withdraw, 'Withdraw', { token: this.scrap.address, tokenId: BN1, tokenValue: BN1, recipient: owner });
  }); */

  it('can receive ERC721 tokens with ERC721 receiver interface', async function() {
    await this.part.mintPart(owner, 1, 'https://brpart.erc721.eth/1', { from: owner });

    const origTokenUserNonce = await this.portal.tokenUserNonce(this.part.address, owner);
    
    await this.part.safeTransferFrom(owner, this.portal.address, 1);

    const newTokenUserNonce = await this.portal.tokenUserNonce(this.part.address, owner);
    const partOwner = await this.part.ownerOf(1);

    expect(origTokenUserNonce.lt(newTokenUserNonce)).to.be.true;
    expect(partOwner.toLowerCase() === this.portal.address.toLowerCase()).to.be.true;
  });

  it('can receive ERC721 tokens with ERC721 deposit interface', async function() {
    await this.part.mintPart(owner, 2, 'https://brpart.erc721.eth/2', { from: owner });
    await this.part.approve(this.portal.address, 2);

    const origTokenUserNonce = await this.portal.tokenUserNonce(this.part.address, owner);
    
    await this.portal.deposit(this.part.address, 2);

    const newTokenUserNonce = await this.portal.tokenUserNonce(this.part.address, owner);
    const partOwner = await this.part.ownerOf(2);

    expect(origTokenUserNonce.lt(newTokenUserNonce)).to.be.true;
    expect(partOwner.toLowerCase() === this.portal.address.toLowerCase()).to.be.true;
  });

  it('allows withdrawal of ERC721 tokens via withdraw interface', async function() {
    const erc721Withrdaw = await this.portal.withdraw(this.part.address, 1, owner);

    //console.log("erc721Withrdaw", JSON.stringify(erc721Withrdaw));

    expectEvent(erc721Withrdaw, 'Withdraw', { token: this.part.address, tokenId: BN1, tokenValue: BN1, recipient: owner });

    const destPartOwner = await this.part.ownerOf(1);

    expect(destPartOwner === owner).to.be.true;
  });

  it("allows allows and increases user's nonce when performing meta transaction", async function() {
    await this.part.mintPart(user1, 3, 'https://brpart.erc721.eth/3', { from: owner });
    await this.part.approve(this.portal.address, 3, {from: user1});

    await this.part.safeTransferFrom(user1, this.portal.address, 3, {from: user1});

    expect(await this.part.ownerOf(3)).to.be.equal(this.portal.address);

    //create a meta transaction for user1 to execute
    const functionSig = await web3.eth.abi.encodeFunctionCall({
      name: 'withdraw', 
      type: 'function', 
      inputs: [
        {
          "name": "token",
          "type": "address"
        },
        {
          "name": "tokenIdOrAmount",
          "type": "uint256"
        },
        {
          "name": "recipient",
          "type": "address"
        }
      ]
    }, [ this.part.address, 3, user1 ]);

    const userNonce = await this.portal.getNonce(user1);
    const chainId = await this.portal.getChainId();

    const domainData = {
      name: await this.portal.name(),
      version: await this.portal.version(),
      verifyingContract: this.portal.address,
      salt: "0x" + web3.utils.toHex(chainId).replace("0x", "").padStart(64, "0")
    };
  
    const dataToSign = {
      types: {
        EIP712Domain: domainType,
        MetaTransaction: metaTransactionType
      },
      primaryType: 'MetaTransaction',
      domain: domainData,
      message: {
        nonce: userNonce,
        from: owner,
        functionSignature: functionSig
      }
    };

    const wallet = ethers.Wallet.fromMnemonic(MNEMONIC);
    const signature = sigUtil.signTypedData(web3.utils.hexToBytes(wallet.privateKey), { data: dataToSign });
    const { r, s, v } = getSignatureParameters(signature);

    await this.portal.executeMetaTransaction(owner, functionSig, r, s, v, { from: user1 });

    //the token should return to user's possession
    expect(await this.part.ownerOf(3)).to.be.equal(user1);
  });

  //use this when last test is checking for event
  //because somehow, a post clean up fails w/o next test function
  it('last call cleanup', async function() {
    expect(true).to.be.true;
  });

});