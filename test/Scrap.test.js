const ethutil = require('ethereumjs-util');

const { expect, assert } = require('chai');
const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

const { getTests, getMintableTests, getBurnableTests, getPausableTests, getAccessControlTests } = require("../utils/tests/ERC20");

require('dotenv').config();

const Scrap = artifacts.require('Scrap');

const BN0 = new BN("0");
const BN1 = new BN('1');
const BN10 = new BN('10');
const BN20 = new BN('20');

const sign = async (web3, signer, msg) => {
  const ret = await web3.eth.sign(msg, signer);

  const sig = ret.slice(2);
  const r = ethutil.toBuffer('0x' + sig.substring(0, 64));
  const s = ethutil.toBuffer('0x' + sig.substring(64, 128));
  let v = parseInt(sig.substring(128, 130), 16);

  if (v === 0 || v === 1) {
    v += 27;
  }

  return Buffer.concat([
    r,
    s,
    ethutil.toBuffer(v)
  ]);
}

const createReward = async (signer, rewardId, recipient, amount) => {
  const address = recipient.toLowerCase();
  const hash = web3.utils.soliditySha3(rewardId, address, amount);

  let signature = null;
  try {
    signature = await sign(web3, signer, hash);
  } catch (err) {
    console.error(err);
  }

  return signature;
}

contract('Scrap', async (accounts) => {
  
  before(async function () {
    this.scrap = await Scrap.deployed(); 
  });

  it('passes all ERC20 mintable tests', async function() {
    const mintableTests = getMintableTests();
    const testKeys = Object.keys(mintableTests);
    
    for(let i = 0; i < testKeys.length; i++){
      const subject = testKeys[i];
      const result = await mintableTests[subject].fn(this.scrap, accounts);
      expect(result === mintableTests[subject].expect).to.be.true;
    }
    
  });

  it('passes all ERC20 burnable tests', async function() {
    const tests = getBurnableTests();
    const testKeys = Object.keys(tests);
    
    for(let i = 0; i < testKeys.length; i++){
      const subject = testKeys[i];
      const result = await tests[subject].fn(this.scrap, accounts);
      expect(result === tests[subject].expect).to.be.true;
    }
    
  });

  it('passes all ERC20 pausable tests', async function() {
    const tests = getPausableTests();
    const testKeys = Object.keys(tests);
    
    for(let i = 0; i < testKeys.length; i++){
      const subject = testKeys[i];
      const result = await tests[subject].fn(this.scrap, accounts);
      expect(result === tests[subject].expect).to.be.true;
    }
    
  });

  it('passes all ERC20 access control tests', async function() {
    const tests = getAccessControlTests();
    const testKeys = Object.keys(tests);
    
    for(let i = 0; i < testKeys.length; i++){
      const subject = testKeys[i];
      const result = await tests[subject].fn(this.scrap, accounts);
      expect(result === tests[subject].expect).to.be.true;
    }
    
  });

  it('passes all ERC20 standard tests', async function() {
    const tests = getTests();
    const testKeys = Object.keys(tests);
    
    for(let i = 0; i < testKeys.length; i++){
      const subject = testKeys[i];
      const result = await tests[subject].fn(this.scrap, accounts);
      expect(result === tests[subject].expect).to.be.true;
    }
    
  });
  
  //test pausable
  it('will not allow mint/burn/transfer/approve when paused', async function() {
    const [ admin, user1 ] = accounts;

    // pause contract
    await this.scrap.pause();

    try {
      await this.scrap.mint(user1, BN1, { from: admin });
      expect.fail("minting while paused succeeded when it should not");
    } catch (err) {
      expect(err.toString().indexOf("token transfer while paused")).to.be.above(0);
    }
  });

  //test access control for MINTER
  it('will not allow mint/burn when user has no MINTER_ROLE', async function() {
    const [ admin, user1, user2 ] = accounts;

    if(await this.scrap.paused()){
      await this.scrap.unpause();
    }

    const user2IsMinter = await this.scrap.hasRole(web3.utils.sha3("MINTER_ROLE"), user2);
    expect(user2IsMinter).to.be.false;

    try {
      await this.scrap.mint(user1, BN1, { from: user2 });
      expect.fail("minting without MINTER_ROLE succeeded when it should not");
    } catch (err) {
      expect(err.toString().indexOf("must have minter role to mint")).to.be.above(0);
    }
  });

  //test claim reward?
  it('will allow reward claim when unpaused', async function() {
    const [ admin, user1, user2 ] = accounts;

    if(await this.scrap.paused()){
      await this.scrap.unpause();
    }

    const rewardId = Date.now().toString();
    const rewardSignature = await createReward(admin, rewardId, user1, BN10);
    const user2OrigBal = await this.scrap.balanceOf(user1);

    try {
      await this.scrap.claimReward(rewardId, BN10, rewardSignature, { from: user1 });
      const user2OrigBalNew = await this.scrap.balanceOf(user1);
      expect(user2OrigBalNew.eq(user2OrigBal.add(BN10)));
    } catch(err){
      expect.fail(err.toString());
    }
  });

  //test claim reward?
  it('will disallow reward claim when paused', async function() {
    const [ admin, user1, user2 ] = accounts;

    if(!(await this.scrap.paused())){
      await this.scrap.pause();
    }

    const rewardId = Date.now().toString();
    const rewardSignature = await createReward(admin, rewardId, user1, BN10);
    
    try {
      await this.scrap.claimReward(rewardId, BN10, rewardSignature, { from: user1 });
      expect.fail("able to claim while paused");
    } catch(err){
      expect(err.toString().indexOf("token transfer while paused")).to.be.above(0);
    }
  });
});