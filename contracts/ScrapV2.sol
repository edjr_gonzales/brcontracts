// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { ERC20 } from "@openzeppelin/contracts/token/ERC20/ERC20.sol";

import { ERC20Upgradeable } from "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import { IERC20Upgradeable } from "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";

import { ERC20PresetMinterPauserUpgradeable } from "@openzeppelin/contracts-upgradeable/token/ERC20/presets/ERC20PresetMinterPauserUpgradeable.sol";
import { ERC20BurnableUpgradeable } from "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20BurnableUpgradeable.sol";

import { SafeMathUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import { AddressUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/AddressUpgradeable.sol";
import { ECDSAUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";

import { ScrapUpgradeable } from "./upgradeable/ScrapUpgradeable.sol";

contract ScrapV2 is ScrapUpgradeable {
  using SafeMathUpgradeable for uint256;
  using AddressUpgradeable for address;
  using ECDSAUpgradeable for bytes32;

  //original scrap token address
  address private _original;

  //old supply count from old contract
  uint256 private _initSupply; 

  mapping (address => uint256) private _prevBalTransferred; // flags to set if transferred
  mapping (address => bool) private _balanceTransferred;    // might be zero even if balance was transferred, avoid confusion using _prevBalTransferred flags

  mapping (address => mapping (address => uint256)) private _prevAllowanceTransferred;
  mapping (address => mapping (address => bool)) private _allowanceTransferred;

  mapping(bytes32 => bool) public completedOrderIds;

  function initializeScrapV2(address original, string memory name, string memory symbol, string memory version) public initializer {
    require(original.isContract(), "Address Error");

    ScrapUpgradeable.initialize(name, symbol, version);

    _original = original;
    _initSupply = IERC20(original).totalSupply();
  }

  /**
    * @dev See {IERC20-totalSupply}.
    */
  function totalSupply() public view override(ERC20Upgradeable) returns (uint256) {
    return super.totalSupply().add(_initSupply);
  }

  /**
    * @dev See {IERC20-balanceOf}.
    */
  function balanceOf(address account) public view override(ERC20Upgradeable) returns (uint256) {
    uint256 oldContractBal = IERC20(_original).balanceOf(account);

    if(_balanceTransferred[account]){ //if balance was recorded transferred before
      if(oldContractBal != _prevBalTransferred[account]){ //if we accidentally minted using old contract, we must be able to carry it here
        if(oldContractBal > _prevBalTransferred[account]){
          uint256 balDiff = oldContractBal - _prevBalTransferred[account];
          return super.balanceOf(account).add(balDiff);
        } else {
          uint256 balDiff = _prevBalTransferred[account] - oldContractBal;
          return super.balanceOf(account).add(balDiff);
        }
        
      } else {
        return super.balanceOf(account);
      }
    } else {
      return oldContractBal;
    }
  }

  /**
     * @dev Creates `amount` new tokens for `to`.
     *
     * See {ERC20-_mint}.
     *
     * Requirements:
     *
     * - the caller must have the `MINTER_ROLE`.
     */
  function mint(address to, uint256 amount) public virtual override(ERC20PresetMinterPauserUpgradeable) {
    _balanceFwd(to);

    //perform actual mint
    super.mint(to, amount);
  }

  /**
    * @dev Destroys `amount` tokens from the caller.
    *
    * See {ERC20-_burn}.
    */
  function burn(uint256 amount) public virtual override(ERC20BurnableUpgradeable) {
    _balanceFwd(_msgSender());
    
    super.burn(amount);
  }

  /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller's
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for ``accounts``'s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public virtual override(ERC20BurnableUpgradeable) {
      address currSender = _msgSender();

      _balanceFwd(account);

      if(currSender != account){
        _balanceFwd(currSender);
        _allowanceFwd(currSender, account);
      }

      super.burnFrom(account, amount);
    }

  /**
    * @dev See {IERC20-transfer}.
    *
    * Requirements:
    *
    * - `recipient` cannot be the zero address.
    * - the caller must have a balance of at least `amount`.
    */
  function transfer(address recipient, uint256 amount) public virtual override(ERC20Upgradeable) returns (bool) {
      _balanceFwd(_msgSender());
      _balanceFwd(recipient);
      
      return super.transfer(recipient, amount);
  }

  /**
    * @dev See {IERC20-transferFrom}.
    *
    * Emits an {Approval} event indicating the updated allowance. This is not
    * required by the EIP. See the note at the beginning of {ERC20};
    *
    * Requirements:
    * - `sender` and `recipient` cannot be the zero address.
    * - `sender` must have a balance of at least `amount`.
    * - the caller must have allowance for ``sender``'s tokens of at least
    * `amount`.
    */
  function transferFrom(address sender, address recipient, uint256 amount) public virtual override(ERC20Upgradeable) returns (bool) {
    address currSender = _msgSender();

    _balanceFwd(currSender);
    _balanceFwd(recipient);
    
    if(currSender != sender){
      _balanceFwd(sender);
      _allowanceFwd(sender, currSender);
    }

    return super.transferFrom(sender, recipient, amount);
  }

  /**
    * @dev See {IERC20-allowance}.
    */
  function allowance(address owner, address spender) public view virtual override(ERC20Upgradeable) returns (uint256) {
    if(!_allowanceTransferred[owner][spender]){
      uint256 oldContractAllowance = IERC20(_original).allowance(owner, spender);
      return oldContractAllowance;

    } else {
      return super.allowance(owner, spender);
    }
  }

  /**
    * @dev See {IERC20-approve}.
    *
    * Requirements:
    *
    * - `spender` cannot be the zero address.
    */
  function approve(address spender, uint256 amount) public virtual override(ERC20Upgradeable) returns (bool) {
    _approve(_msgSender(), spender, amount);
    _allowanceTransferred[_msgSender()][spender] = true;
    return true;
  }

  /**
    * @dev Atomically increases the allowance granted to `spender` by the caller.
    *
    * This is an alternative to {approve} that can be used as a mitigation for
    * problems described in {IERC20-approve}.
    *
    * Emits an {Approval} event indicating the updated allowance.
    *
    * Requirements:
    *
    * - `spender` cannot be the zero address.
    */
  function increaseAllowance(address spender, uint256 addedValue) public virtual override(ERC20Upgradeable) returns (bool) {
    _allowanceFwd(_msgSender(), spender);

    return super.increaseAllowance(spender, addedValue);
  }

  /**
    * @dev Atomically decreases the allowance granted to `spender` by the caller.
    *
    * This is an alternative to {approve} that can be used as a mitigation for
    * problems described in {IERC20-approve}.
    *
    * Emits an {Approval} event indicating the updated allowance.
    *
    * Requirements:
    *
    * - `spender` cannot be the zero address.
    * - `spender` must have allowance for the caller of at least
    * `subtractedValue`.
    */
  function decreaseAllowance(address spender, uint256 subtractedValue) public virtual override(ERC20Upgradeable) returns (bool) {
    _allowanceFwd(_msgSender(), spender);

    return super.decreaseAllowance(spender, subtractedValue);
  }

  function supportsInterface(bytes4 interfaceId) public view virtual override returns (bool) {
    return type(IERC20).interfaceId == interfaceId || type(IERC20Upgradeable).interfaceId == interfaceId || super.supportsInterface(interfaceId);
  }

  /** Scrap Overrides to use overridden ERC20 methods */
  function claimReward(
      uint256 rewardId,
      uint256 amount,
      bytes memory signature
  ) public override returns (bool) {
      require(
          completedRequests[rewardId] == false,
          "Claimed"
      );

      address signer =
          keccak256(abi.encodePacked(rewardId, _msgSender(), amount))
              .toEthSignedMessageHash()
              .recover(signature);
      require(signer == owner(), "Signer Invalid");

      _balanceFwd(_msgSender());
      _mint(_msgSender(), amount);
      completedRequests[rewardId] = true;

      emit RewardClaimed(rewardId);

      return true;
  }

  function withdrawOrderPayment(
      address from,
      bytes32 orderId,
      uint256 amount
  ) public override onlyOwner {
      require(
          completedOrderIds[orderId] == false,
          "Claimed"
      );

      transferFrom(from, owner(), amount);
      completedOrderIds[orderId] = true;
      
      emit OrderPayment(orderId);
  }

  /** Internals */

  /**
   * Transfers the account's balance from old token into this token, if not done so.
   */
  function _balanceFwd(address account)  internal virtual {
    require(account != address(0), "account is zero");

    uint256 oldContractBalance = IERC20(_original).balanceOf(account);
    uint256 oldContractSupply = IERC20(_original).totalSupply();

    if(oldContractSupply > _initSupply){
      //token got minted from our last check, carry over the difference
      _initSupply = _initSupply.add(oldContractSupply - _initSupply);
    } else if(oldContractSupply < _initSupply) {
      //token got burned from our last check, carry over the difference
      _initSupply = _initSupply.sub(_initSupply - oldContractSupply);
    } else {
      //seriously, stop touching the old contract. leave it as it is!!!
    }

    //check if this accounts balance from last token has been transferred to this token
    if(_balanceTransferred[account]){
      if(oldContractBalance != _prevBalTransferred[account]){
        if(oldContractBalance > _prevBalTransferred[account]){
          uint256 balDiff = oldContractBalance - _prevBalTransferred[account];
          _mint(account, balDiff);
        } else {
          uint256 balDiff = _prevBalTransferred[account] - oldContractBalance;
          _burn(account, balDiff);
        }
        
        _prevBalTransferred[account] = oldContractBalance;
      } else {
        //no further actions
      }

    } else {
      uint256 bal = IERC20(_original).balanceOf(account);

      //mint
      if(bal > 0){
        _mint(account, bal);
        
        // update previous supply valuu because this value has been migrated to this contract now
        _initSupply = _initSupply.sub(bal);
      }

      _prevBalTransferred[account] = bal;
    }

    if(!_balanceTransferred[account]){
      //set flag so it marks checked
      _balanceTransferred[account] = true;
    }
  }

  /**
   * Transfers the account's allowance from old token into this token, if not done so.
   */
  function _allowanceFwd(address sponsor, address spender)  internal virtual {
    require(spender != address(0), "spender is zero");
    require(sponsor != address(0), "sponsor is zero");

    if(!_allowanceTransferred[sponsor][spender]){
      uint256 spender_allowance = IERC20(_original).allowance(sponsor, spender);

      //set approval
      if(spender_allowance > 0){
        _approve(sponsor, spender, spender_allowance);
      }
    }

    if(!_allowanceTransferred[sponsor][spender]){
      _allowanceTransferred[sponsor][spender] = true;
    }
  }

}
