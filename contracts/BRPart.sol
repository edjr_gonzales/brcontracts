// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { ERC721 } from "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import { ERC721Pausable } from "@openzeppelin/contracts/token/ERC721/extensions/ERC721Pausable.sol";
import { Ownable } from "@openzeppelin/contracts/access/Ownable.sol";
import { AccessControl } from "@openzeppelin/contracts/access/AccessControl.sol";
import { Context } from "@openzeppelin/contracts/utils/Context.sol";
import { ECDSA } from "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import { IERC721Metadata } from "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";

import { EIP712MetaTransaction } from "./lib/eip712/EIP712MetaTransaction.sol";
import { ContextMixin } from "./lib/common/ContextMixin.sol";
import { AccessControlMixin } from "./lib/common/AccessControlMixin.sol";

contract BRPart is
    AccessControlMixin,
    ContextMixin,
    EIP712MetaTransaction,
    ERC721Pausable
{
    using ECDSA for bytes32;

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    address private _owner;

    // Base URI
    // string private _baseURI;

    // Optional mapping for token URIs
    mapping(uint256 => string) private _tokenURIs;

    constructor(
        string memory name,
        string memory symbol,
        string memory version
    ) ERC721(name, symbol) {
        _setupContractId("EIP712BRPart");
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _setupRole(MINTER_ROLE, _msgSender());
        _initializeEIP712(name, version);

        _owner = _msgSender();
    }

    // This is to support Native meta transactions
    // never use msg.sender directly, use _msgSender() instead
    function _msgSender()
        internal
        override(Context, EIP712MetaTransaction)
        view
        returns (address sender)
    {
        return super.msgSender();
    }

    function disable() external {
        _pause();
    }

    function enable() external {
       _unpause();
    }

    function addMinter(address account) external {
        grantRole(MINTER_ROLE, account);
    }

    function delMinter(address account) external {
        revokeRole(MINTER_ROLE, account);
    }

    function isMinter(address account) external view returns (bool) {
        return hasRole(MINTER_ROLE, account);
    }

    /**
     * @dev Internal function to set the token URI for a given token.
     *
     * Reverts if the token ID does not exist.
     *
     * TIP: if all token IDs share a prefix (e.g. if your URIs look like
     * `http://api.myproject.com/token/<id>`), use {_setBaseURI} to store
     * it and save gas.
     */
    function _setTokenURI(uint256 tokenId, string memory _tokenURI) internal {
        require(_exists(tokenId), "ERC721Metadata: URI set of nonexistent token");
        _tokenURIs[tokenId] = _tokenURI;
    }

    function tokenURI(uint256 tokenId) public view virtual override(ERC721) returns (string memory) {
        require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");

        string memory _tokenURI = _tokenURIs[tokenId];

        // Even if there is a base URI, it is only appended to non-empty token-specific URIs
        if (bytes(_tokenURI).length == 0) {
            return "";
        } else {
            // abi.encodePacked is being used to concatenate strings
            return string(abi.encodePacked(_baseURI(), _tokenURI));
        }
    }

    function setTokenURI(
        uint256 tokenId,
        string calldata uri
    ) external withRole(DEFAULT_ADMIN_ROLE) returns (bool) {
        _setTokenURI(tokenId, uri);
        return true;
    }

    function mintPart(
        address to,
        uint256 tokenId,
        string calldata uri
    ) external whenNotPaused withRole(MINTER_ROLE) returns (bool) {
        _mint(to, tokenId);
        _setTokenURI(tokenId, uri);
        return true;
    }

    function mintPartSigned(
        address to,
        uint256 tokenId,
        string calldata uri,
        bytes calldata signature
    ) external whenNotPaused withRole(MINTER_ROLE) returns (bool) {
        address signer = keccak256(abi.encodePacked(tokenId, _msgSender(), uri))
            .toEthSignedMessageHash()
            .recover(signature);
        require(signer == _owner, "Incorrect message signer");

        _mint(to, tokenId);
        _setTokenURI(tokenId, uri);
        return true;
    }

    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId
    ) public override whenNotPaused {
        ERC721.safeTransferFrom(from, to, tokenId, "");
    }

    function safeTransferFrom(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    ) public override whenNotPaused {
        ERC721.safeTransferFrom(from, to, tokenId, _data);
    }

    function transferTokenFrom(
        address from,
        address to,
        uint256 tokenId
    ) public {
        transferFrom(from, to, tokenId);
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(AccessControl, ERC721) returns (bool) {
        return super.supportsInterface(interfaceId);
    }
}
