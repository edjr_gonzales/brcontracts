pragma solidity ^0.6.6;

import {Ownable} from "@openzeppelin/contracts/access/Ownable.sol";
import {ERC20PresetMinterPauser} from "@openzeppelin/contracts/presets/ERC20PresetMinterPauser.sol";
import {SafeMath} from "@openzeppelin/contracts/math/SafeMath.sol";
import {Math} from "@openzeppelin/contracts/math/Math.sol";
import {ECDSA} from "@openzeppelin/contracts/cryptography/ECDSA.sol";

import {EIP712MetaTransaction} from "./lib/eip712/EIP712MetaTransaction.sol";
import {ContextMixin} from "./lib/common/ContextMixin.sol";
import {MinterMixin} from "./lib/common/MinterMixin.sol";
import {AdminMixin} from "./lib/common/AdminMixin.sol";
import {AccessControlMixin} from "./lib/common/AccessControlMixin.sol";
import {IERC20Receiver} from "./lib/erc20/IERC20Receiver.sol";

contract Scrap is
    ERC20PresetMinterPauser,
    Ownable,
    AccessControlMixin,
    EIP712MetaTransaction,
    ContextMixin,
    MinterMixin,
    AdminMixin
{
    using SafeMath for uint256;
    using ECDSA for bytes32;

    bytes4 private constant _ERC20_RECEIVER_INTERFACE = bytes4(keccak256("onERC20Received(address,address,uint256)"));

    event RewardClaimed(uint256 id);

    event OrderPayment(bytes32 orderId);

    mapping(uint256 => bool) public completedRequests;

    constructor(
        string memory name,
        string memory symbol,
        string memory version
    ) public ERC20PresetMinterPauser(name, symbol) {
        _setupContractId("SCRAP");
        _initializeEIP712(name, version);
    }

    // This is to support Native meta transactions
    // never use msg.sender directly, use _msgSender() instead
    function _msgSender()
        internal
        view
        override
        returns (address payable sender)
    {
        return ContextMixin.msgSender();
    }

    /**
     * @dev Internal function to invoke {IERC20Receiver-onERC20Received} on a target address.
     * The call is not executed if the target address is not a contract.
     *
     * @param from address representing the previous owner of the tokens
     * @param to target address that will receive the tokens
     * @param tokenAmount uint256 amount of tokens to be transferred
     * @return bool whether the call correctly returned the expected magic value
     */
    function _checkOnERC20Received(address from, address to, uint256 tokenAmount)
        private returns (bool)
    {
        if (!to.isContract()) {
            return true;
        }
        bytes memory returndata = to.functionCall(abi.encodeWithSelector(
            IERC20Receiver(to).onERC20Received.selector,
            _msgSender(),
            from,
            tokenAmount
        ), "ERC20: transfer to non ERC20Receiver implementer");
        bytes4 retval = abi.decode(returndata, (bytes4));
        return (retval == _ERC20_RECEIVER_INTERFACE);
    }

    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        ERC20PresetMinterPauser.transfer(recipient, amount);
        require(_checkOnERC20Received(address(0), recipient, amount), "ERC20: transfer to non ERC20Receiver implementer");
        return true;
    }

    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        ERC20PresetMinterPauser.transferFrom(sender, recipient, amount);
        require(_checkOnERC20Received(address(0), recipient, amount), "ERC20: transfer to non ERC20Receiver implementer");
        return true;
    }

    function mint(address to, uint256 amount) public virtual override {
        ERC20PresetMinterPauser.mint(to, amount);
        require(_checkOnERC20Received(address(0), to, amount), "ERC20: transfer to non ERC20Receiver implementer");
    }

    function claimReward(
        uint256 rewardId,
        uint256 amount,
        bytes memory signature
    ) public returns (bool) {
        require(
            completedRequests[rewardId] == false,
            "Request already completed."
        );

        address signer =
            keccak256(abi.encodePacked(rewardId, _msgSender(), amount))
                .toEthSignedMessageHash()
                .recover(signature);
        require(signer == owner(), "Incorrect message signer");

        _mint(_msgSender(), amount);
        completedRequests[rewardId] = true;

        emit RewardClaimed(rewardId);

        return true;
    }

    function withdrawOrderPayment(
        address from,
        bytes32 orderId,
        uint256 amount
    ) public onlyOwner {
        transferFrom(from, owner(), amount);
        emit OrderPayment(orderId);
    }

    function withdrawEther() external onlyOwner {
        _msgSender().transfer(address(this).balance);
    }
}
