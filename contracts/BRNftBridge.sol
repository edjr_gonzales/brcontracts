// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { IERC721 } from "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import { Ownable } from "@openzeppelin/contracts/access/Ownable.sol";
import { Context } from "@openzeppelin/contracts/utils/Context.sol";

import {EIP712MetaTransaction} from "./lib/eip712/EIP712MetaTransaction.sol";
import {ContextMixin} from "./lib/common/ContextMixin.sol";
import { AccessControlMixin } from "./lib/common/AccessControlMixin.sol";

contract BRNftBridge is
    AccessControlMixin,
    ContextMixin,
    EIP712MetaTransaction
{
    bytes32 public constant BRIDGE_CONTROLLER_ROLE = keccak256(
        "BRIDGE_CONTROLLER_ROLE"
    );

    mapping(address => mapping(uint256 => uint256)) private _erc721BanDuration;

    bool private _closed;
    string private _name;
    string private _symbol;
    address private _owner;

    event Deposit(
        address indexed token,
        uint256 indexed tokenId,
        address indexed user
    );
    event Withdraw(
        address indexed token,
        uint256 indexed tokenId,
        address indexed user
    );
    event Banned(
        address indexed token,
        uint256 indexed tokenId,
        uint256 indexed until
    );
    event BanLifted(address indexed token, uint256 indexed tokenId);

    constructor(
        string memory name,
        string memory symbol,
        string memory version
    ) public {
        _closed = false;
        _name = name;
        _symbol = symbol;

        _setupContractId("EIP712BRNftBridge");
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _setupRole(BRIDGE_CONTROLLER_ROLE, _msgSender());
        _initializeEIP712(name, version);

        _owner = _msgSender();
    }

    modifier canControlBridge() {
        require(
            hasRole(BRIDGE_CONTROLLER_ROLE, _msgSender()) ||
                _msgSender() == _owner,
            "BRNftBridge: Not Allowed."
        );
        _;
    }

    modifier bridgeIsOpen() {
        require(!_closed, "BRNftBridge: Closed.");
        _;
    }

    modifier isErc721(address token) {
        require(
            IERC721(token).supportsInterface(0x80ac58cd),
            "BRNftBridge: Is not an ERC721 token as you were led to believe."
        );
        _;
    }

    modifier isNotBlacklisted(address token, uint256 tokenId) {
        require(
            _erc721BanDuration[token][tokenId] < block.timestamp,
            "BRNftBridge: This particular token is prohibited on doing cross trade."
        );
        _;
    }

    // This is to support Native meta transactions
    // never use msg.sender directly, use _msgSender() instead
    function _msgSender()
        internal
        override(Context, EIP712MetaTransaction)
        view
        returns (address sender)
    {
        return super.msgSender();
    }

    function name() public view returns (string memory) {
        return _name;
    }

    function symbol() public view returns (string memory) {
        return _symbol;
    }

    function openBridge() external canControlBridge {
        _closed = false;
    }

    function closeBridge() external canControlBridge {
        _closed = true;
    }

    function addBridgeAdmin(address user) external withRole(DEFAULT_ADMIN_ROLE) {
        grantRole(BRIDGE_CONTROLLER_ROLE, user);
    }

    function delBridgeAdmin(address user) external withRole(DEFAULT_ADMIN_ROLE) {
        revokeRole(BRIDGE_CONTROLLER_ROLE, user);
    }

    function blockToken(
        address token,
        uint256 tokenId,
        uint256 duration
    ) external canControlBridge isErc721(token) {
        uint256 bannedUntil = block.timestamp + duration;
        _erc721BanDuration[token][tokenId] = bannedUntil;

        emit Banned(token, tokenId, bannedUntil);
    }

    function permitToken(address token, uint256 tokenId)
        external
        canControlBridge
        isErc721(token)
    {
        _erc721BanDuration[token][tokenId] = 0;

        emit BanLifted(token, tokenId);
    }

    function isTokenBlocked(address token, uint256 tokenId)
        external
        view
        isErc721(token)
        returns (bool)
    {
        return _erc721BanDuration[token][tokenId] > block.timestamp;
    }

    function isContractIERC721(address token) external view returns (bool) {
        return IERC721(token).supportsInterface(0x80ac58cd);
    }

    function isBridgeOpen() external view returns (bool) {
        return !_closed;
    }

    function deposit(address token, uint256 tokenId)
        external
        bridgeIsOpen
        isErc721(token)
        isNotBlacklisted(token, tokenId)
    {
        IERC721(token).transferFrom(_msgSender(), address(this), tokenId);

        emit Deposit(token, tokenId, _msgSender());
    }

    function withdraw(
        address token,
        uint256 tokenId,
        address user
    ) external canControlBridge isErc721(token) {
        IERC721(token).transferFrom(address(this), user, tokenId);

        emit Withdraw(token, tokenId, user);
    }
}
