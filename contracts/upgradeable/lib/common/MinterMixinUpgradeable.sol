// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { ERC20PresetMinterPauserUpgradeable } from "@openzeppelin/contracts-upgradeable/token/ERC20/presets/ERC20PresetMinterPauserUpgradeable.sol";

abstract contract MinterMixinUpgradeable is ERC20PresetMinterPauserUpgradeable {
    function setMinter(address minterAcc, bool isGrant) public {
      if (isGrant){
        grantRole(MINTER_ROLE, minterAcc);
      } else {
        revokeRole(MINTER_ROLE, minterAcc);
      }
    }

    function isMinter(address minterAcc) public view returns (bool) {
      return hasRole(MINTER_ROLE, minterAcc);
    }
}
