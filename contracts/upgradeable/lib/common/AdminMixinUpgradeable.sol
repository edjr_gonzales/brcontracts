// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { AccessControlUpgradeable } from "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";

abstract contract AdminMixinUpgradeable is AccessControlUpgradeable {
    function setAdmin(address adminAcc, bool isGrant) public {
      if (isGrant){
        grantRole(DEFAULT_ADMIN_ROLE, adminAcc);
      } else {
        revokeRole(DEFAULT_ADMIN_ROLE, adminAcc);
      }
    }

    function isAdmin(address adminAcc) public view returns (bool) {
      return hasRole(DEFAULT_ADMIN_ROLE, adminAcc);
    }
}
