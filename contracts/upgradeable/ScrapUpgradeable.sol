// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { OwnableUpgradeable } from "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import { AccessControlUpgradeable } from "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import { AccessControlEnumerableUpgradeable } from "@openzeppelin/contracts-upgradeable/access/AccessControlEnumerableUpgradeable.sol";
import { ContextUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol";
import { ERC20PresetMinterPauserUpgradeable } from "@openzeppelin/contracts-upgradeable/token/ERC20/presets/ERC20PresetMinterPauserUpgradeable.sol";
import { SafeMathUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import { MathUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/math/MathUpgradeable.sol";
import { ECDSAUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";

import { EIP712MetaTransactionUpgradeable } from "./lib/eip712/EIP712MetaTransactionUpgradeable.sol";
import { ContextMixin } from "../lib/common/ContextMixin.sol";
import { MinterMixinUpgradeable } from "./lib/common/MinterMixinUpgradeable.sol";
import { AdminMixinUpgradeable } from "./lib/common/AdminMixinUpgradeable.sol";
import { AccessControlMixinUpgradeable } from "./lib/common/AccessControlMixinUpgradeable.sol";

contract ScrapUpgradeable is
    OwnableUpgradeable,
    ContextMixin,
    EIP712MetaTransactionUpgradeable,
    ERC20PresetMinterPauserUpgradeable,
    MinterMixinUpgradeable,
    AdminMixinUpgradeable
{
    using SafeMathUpgradeable for uint256;
    using ECDSAUpgradeable for bytes32;

    event RewardClaimed(uint256 id);

    event OrderPayment(bytes32 orderId);

    mapping(uint256 => bool) public completedRequests;

    function initialize(string memory name, string memory symbol, string memory version) public virtual initializer {
        _initializeEIP712(name, version);
        __Ownable_init_unchained();
        super.initialize(name, symbol);
    }

    // This is to support Native meta transactions
    // never use msg.sender directly, use _msgSender() instead
    function _msgSender()
        internal
        view
        override(ContextUpgradeable, EIP712MetaTransactionUpgradeable)
        returns (address sender)
    {
        return ContextMixin.msgSender();
    }

    function claimReward(
        uint256 rewardId,
        uint256 amount,
        bytes memory signature
    ) public virtual whenNotPaused returns (bool) {
        require(
            completedRequests[rewardId] == false,
            "Request already completed."
        );

        address signer =
            keccak256(abi.encodePacked(rewardId, _msgSender(), amount))
                .toEthSignedMessageHash()
                .recover(signature);
        require(signer == owner(), "Incorrect message signer");

        _mint(_msgSender(), amount);
        completedRequests[rewardId] = true;

        emit RewardClaimed(rewardId);

        return true;
    }

    function withdrawOrderPayment(
        address from,
        bytes32 orderId,
        uint256 amount
    ) public virtual onlyOwner whenNotPaused {
        transferFrom(from, owner(), amount);
        emit OrderPayment(orderId);
    }

    function withdrawEther() external onlyOwner {
        payable(_msgSender()).transfer(address(this).balance);
    }

    //overrides
    function _setupRole(bytes32 role, address account) internal virtual override(AccessControlUpgradeable, AccessControlEnumerableUpgradeable) {
        super._setupRole(role, account);
    }
    
    function grantRole(bytes32 role, address account) public virtual override(AccessControlUpgradeable, AccessControlEnumerableUpgradeable) {
        super.grantRole(role, account);
    }

    function renounceRole(bytes32 role, address account) public virtual override(AccessControlUpgradeable, AccessControlEnumerableUpgradeable) {
        super.renounceRole(role, account);
    }

    function revokeRole(bytes32 role, address account) public virtual override(AccessControlUpgradeable, AccessControlEnumerableUpgradeable) {
        super.revokeRole(role, account);
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(AccessControlUpgradeable, AccessControlEnumerableUpgradeable) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    /**
     * @dev Returns the number of decimals used to get its user representation.
     * For example, if `decimals` equals `2`, a balance of `505` tokens should
     * be displayed to a user as `5,05` (`505 / 10 ** 2`).
     *
     * Tokens usually opt for a value of 18, imitating the relationship between
     * Ether and Wei. This is the value {ERC20} uses, unless this function is
     * overridden;
     *
     * NOTE: This information is only used for _display_ purposes: it in
     * no way affects any of the arithmetic of the contract, including
     * {IERC20-balanceOf} and {IERC20-transfer}.
     */
    function decimals() public view virtual override returns (uint8) {
        return 0;
    }
}