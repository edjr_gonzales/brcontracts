// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import { Ownable } from "@openzeppelin/contracts/access/Ownable.sol";
import { AccessControl } from "@openzeppelin/contracts/access/AccessControl.sol";
import { AccessControlEnumerable } from "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import { Context } from "@openzeppelin/contracts/utils/Context.sol";
import { ERC20PresetMinterPauser } from "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol";
import { SafeMath } from "@openzeppelin/contracts/utils/math/SafeMath.sol";
import { Math } from "@openzeppelin/contracts/utils/math/Math.sol";
import { ECDSA } from "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";

import { EIP712MetaTransaction } from "./lib/eip712/EIP712MetaTransaction.sol";
import { ContextMixin } from "./lib/common/ContextMixin.sol";
import { MinterMixin } from "./lib/common/MinterMixin.sol";
import { AdminMixin } from "./lib/common/AdminMixin.sol";
import { AccessControlMixin } from "./lib/common/AccessControlMixin.sol";

contract Scrap is
    Ownable,
    ContextMixin,
    EIP712MetaTransaction,
    ERC20PresetMinterPauser,
    MinterMixin,
    AdminMixin
{
    using SafeMath for uint256;
    using ECDSA for bytes32;

    event RewardClaimed(uint256 id);

    event OrderPayment(bytes32 orderId);

    mapping(uint256 => bool) public completedRequests;

    constructor(
        string memory name,
        string memory symbol,
        string memory version
    ) ERC20PresetMinterPauser(name, symbol) {
        //_setupContractId("SCRAP");
        _initializeEIP712(name, version);
    }

    // This is to support Native meta transactions
    // never use msg.sender directly, use _msgSender() instead
    function _msgSender()
        internal
        view
        override(Context, EIP712MetaTransaction)
        returns (address sender)
    {
        return ContextMixin.msgSender();
    }

    function claimReward(
        uint256 rewardId,
        uint256 amount,
        bytes memory signature
    ) public returns (bool) {
        require(
            completedRequests[rewardId] == false,
            "Request already completed."
        );

        address signer =
            keccak256(abi.encodePacked(rewardId, _msgSender(), amount))
                .toEthSignedMessageHash()
                .recover(signature);
        require(signer == owner(), "Incorrect message signer");

        _mint(_msgSender(), amount);
        completedRequests[rewardId] = true;

        emit RewardClaimed(rewardId);

        return true;
    }

    function withdrawOrderPayment(
        address from,
        bytes32 orderId,
        uint256 amount
    ) public onlyOwner {
        transferFrom(from, owner(), amount);
        emit OrderPayment(orderId);
    }

    function withdrawEther() external onlyOwner {
        payable(_msgSender()).transfer(address(this).balance);
    }

    //overrides
    function _setupRole(bytes32 role, address account) internal virtual override(AccessControl, AccessControlEnumerable) {
        super._setupRole(role, account);
    }
    
    function grantRole(bytes32 role, address account) public virtual override(AccessControl, AccessControlEnumerable) {
        super.grantRole(role, account);
    }

    function renounceRole(bytes32 role, address account) public virtual override(AccessControl, AccessControlEnumerable) {
        super.renounceRole(role, account);
    }

    function revokeRole(bytes32 role, address account) public virtual override(AccessControl, AccessControlEnumerable) {
        super.revokeRole(role, account);
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(AccessControl, AccessControlEnumerable) returns (bool) {
        return type(IERC20).interfaceId == interfaceId || super.supportsInterface(interfaceId);
    }
}