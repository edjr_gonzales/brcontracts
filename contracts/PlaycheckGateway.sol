// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { SafeMathUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import { AddressUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/AddressUpgradeable.sol";

import { OwnableUpgradeable } from "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import { IERC165 } from "@openzeppelin/contracts/utils/introspection/IERC165.sol";
import { ERC165Upgradeable } from "@openzeppelin/contracts-upgradeable/utils/introspection/ERC165Upgradeable.sol";
import { ContextUpgradeable } from "@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol";
import { AccessControlUpgradeable } from "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";

import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { ERC20Burnable } from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import { IERC20Receiver } from "./lib/erc20/IERC20Receiver.sol";

import { IERC721 } from "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import { IERC721Receiver } from "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";

import { IERC1155 } from "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import { IERC1155Receiver } from "@openzeppelin/contracts/token/ERC1155/IERC1155Receiver.sol";

import { EIP712MetaTransactionUpgradeable } from "./upgradeable/lib/eip712/EIP712MetaTransactionUpgradeable.sol";
import { ToolsMixin } from "./lib/common/ToolsMixin.sol";
import { ContextMixin } from "./lib/common/ContextMixin.sol";
import { AccessControlMixinUpgradeable } from "./upgradeable/lib/common/AccessControlMixinUpgradeable.sol";

contract PlaycheckGateway is
    ToolsMixin,
    ContextMixin,
    IERC721Receiver,
    IERC1155Receiver,
    AccessControlMixinUpgradeable,
    EIP712MetaTransactionUpgradeable
{
    using SafeMathUpgradeable for uint256;
    using AddressUpgradeable for address;

    bytes32 public constant BRIDGE_CONTROLLER_ROLE = keccak256("BRIDGE_CONTROLLER_ROLE"
    );

    bytes4 private constant _ERC20_INTERFACE = type(IERC20).interfaceId;
    bytes4 private constant _ERC721_INTERFACE = type(IERC721).interfaceId;
    
    bytes4 private constant _ERC1155_INTERFACE = type(IERC1155).interfaceId;
    
    //we can prohibit a user's ability to transact per token
    mapping(address => mapping(address => uint256)) private _tokenUserBanDuration;

    mapping(address => mapping(address => uint256)) private _tokenDepositNonces;

    /* wallets/users that will generally be prohibited */
    mapping(address => uint256) private _blackListedWallets;

    /*
        //contract -> partner wallet/account
        //partners and their contracts, where 
        //they will be allowed to claim the 
        //tokens belonging to their contracts

        //this will also contain the allowed 
        //tokens that needs to use the bridge
    */
    mapping(address => address) private _whiteListedTokens;

    bool private _closed;
    string private _name;
    string private _symbol;
    address private _owner;

    event ERC20Deposit(
        address indexed token,
        address indexed source,
        uint256 indexed nonce,
        uint256 tokenAmt,
        uint256 sourceBal
    );

    event Deposit(
        address indexed token,
        address indexed source,
        uint256 indexed nonce,
        uint256 tokenId,
        uint256 tokenValue
    );
    
    event Withdraw(
        address indexed token,
        uint256 indexed tokenId,
        uint256 tokenValue,
        address indexed recipient
    );

    event Banned(
        address indexed token,
        address indexed user,
        uint256 indexed until
    );

    event BlacklistedWallet(
        address indexed user,
        uint256 indexed until
    );

    event BanLifted(address indexed token, address indexed user);

    event PartnerEvent(
        address indexed contractAddress,
        uint256 indexed timestamp,
        address partnerWallet,
        string action
    );

    function canControlBridge() internal {
        require(
            hasRole(DEFAULT_ADMIN_ROLE, _msgSender()) ||
                hasRole(BRIDGE_CONTROLLER_ROLE, _msgSender()) ||
                _msgSender() == _owner,
            "Bridge: Forbidden"
        );
        //_;
    }

    function bridgeIsOpen() internal {
        require(!_closed, "Bridge: Closed");
        //_;
    }

    function isNotContractBlacklisted(address contractAddress, address sender) internal {
        require(
            _tokenUserBanDuration[contractAddress][sender] < block.timestamp,
            "Bridge: Error 1"
        );
        //_;
    }

    function isNotBlacklisted(address sender) internal {
        require(
            _blackListedWallets[sender] < block.timestamp,
            "Bridge: Error 2."
        );
        //_;
    }

    function isContract(address addr) internal {
        require(
            addr.isContract(),
            "Bridge: Not Contract"
        );
        //_;
    }

    function isERC1155(address addr) internal {
        bool itIsERC1155 = false;

        try IERC165(addr).supportsInterface(_ERC1155_INTERFACE) returns (bool _isERC1155) {
            itIsERC1155 = _isERC1155;
        } catch {}

        require(
            itIsERC1155,
            string(
                abi.encodePacked("Bridge: Not ERC1155")
            )
        );
        //_;
    }

    function isContractAllowed(address contractAddress) internal {
        require(
            _whiteListedTokens[contractAddress] != address(0),
            "Bridge: Not Partner"
        );
        //_;
    }

    function initialize(
        string memory name,
        string memory symbol,
        string memory version
    ) public virtual initializer {
        _closed = false;
        _name = name;
        _symbol = symbol;

        _owner = _msgSender();

        _setupRole(DEFAULT_ADMIN_ROLE, _owner);
        _setupRole(BRIDGE_CONTROLLER_ROLE, _owner);

        _setupContractId("PlaycheckGw");
        _initializeEIP712(name, version);
    }

    function name() public view returns (string memory) {
        return _name;
    }

    function symbol() public view returns (string memory) {
        return _symbol;
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(AccessControlUpgradeable, IERC165) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    function switchBridge(bool state) external {
        canControlBridge();

        _closed = state;
    }

    function closeBridge() external {
        canControlBridge();

        _closed = true;
    }

    function addBridgeAdmin(address user) external {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Not Admin");
        grantRole(BRIDGE_CONTROLLER_ROLE, user);
    }

    function delBridgeAdmin(address user) external {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Not Admin");
        require(user != _owner, "Not Owner");
        
        revokeRole(BRIDGE_CONTROLLER_ROLE, user);
    }

    function blockTokenUser(
        address token,
        address user,
        uint256 duration
    ) external {
        canControlBridge();

        require(user != _owner, "Not Owner");

        uint256 bannedUntil = block.timestamp + duration;
        _tokenUserBanDuration[token][user] = bannedUntil;

        emit Banned(token, user, bannedUntil);
    }

    function permitTokenUser(address token, address user)
        external
    {
        canControlBridge();

        _tokenUserBanDuration[token][user] = 0;

        emit BanLifted(token, user);
    }

    function isTokenUserBlocked(address token, address user)
        external
        view
        returns (bool)
    {
        return _tokenUserBanDuration[token][user] > block.timestamp;
    }

    function blockUserWallet(
        address user,
        uint256 duration
    ) external {
        canControlBridge();

        require(user != _owner, "Not Owner");

        uint256 bannedUntil = block.timestamp + duration;
        _blackListedWallets[user] = bannedUntil;

        emit BlacklistedWallet(user, bannedUntil);
    }

    function permitUserWallet(address user)
        external
    {
        canControlBridge();

        _blackListedWallets[user] = 0;
    }

    function isUserWalletBlocked(address user)
        external
        view
        returns (bool)
    {
        return _blackListedWallets[user] > block.timestamp;
    }

    function isBridgeOpen() external view returns (bool) {
        return !_closed;
    }

    function tokenUserNonce(address token, address user) external view returns (uint256) {
        return _tokenDepositNonces[token][user];
    }

    //implement receiver functions from supported EIP token standards
    function onERC721Received(
        address _operator,
        address _from, 
        uint256 _tokenId, 
        bytes calldata _data
      ) 
        external 
        override
        returns(bytes4) {
      
      isNotContractBlacklisted(_msgSender(), _operator);
      isNotContractBlacklisted(_msgSender(), _from);
      isContract(_msgSender());
      bridgeIsOpen();

      address token = _msgSender();

      emit Deposit(_msgSender(), _from, _tokenDepositNonces[token][_from], _tokenId, 1);
      _tokenDepositNonces[token][_from] = _tokenDepositNonces[token][_from].add(1);

      return this.onERC721Received.selector;
    }

    function onERC1155Received(
        address _operator,
        address _from,
        uint256 _id,
        uint256 _value,
        bytes calldata _data
    )
        external
        override
        returns(bytes4) {
      
      isNotContractBlacklisted(_msgSender(), _operator);
      isNotContractBlacklisted(_msgSender(), _from);
      isContract(_msgSender());
      bridgeIsOpen();

      address token = _msgSender();

      emit Deposit(_msgSender(), _from, _tokenDepositNonces[token][_from], _id, _value);
      _tokenDepositNonces[token][_from] = _tokenDepositNonces[token][_from].add(1);

      return this.onERC1155Received.selector; //_ERC1155_RECEIVER_INTERFACE;
    }

    function _batchEmitDeposit(
        address _token,
        address _from,
        uint256[] memory _ids,
        uint256[] memory _values) internal {

        for (uint256 i = 0; i < _ids.length; ++i) {
            emit Deposit(_token, _from, _tokenDepositNonces[_token][_from].add(i), _ids[i], _values[i]);
        }

        _tokenDepositNonces[_token][_from] = _tokenDepositNonces[_token][_from].add(_ids.length);
    }

    function onERC1155BatchReceived(
        address _operator,
        address _from,
        uint256[] calldata _ids,
        uint256[] calldata _values,
        bytes calldata _data
    )
        external
        override
        returns(bytes4) {

        isNotContractBlacklisted(_msgSender(), _operator);
        isNotContractBlacklisted(_msgSender(), _from);
        isContract(_msgSender());
        bridgeIsOpen();

        _batchEmitDeposit(_msgSender(), _from, _ids, _values);
        
        return this.onERC1155BatchReceived.selector; //bytes4(keccak256("onERC1155BatchReceived(address,address,uint256[],uint256[],bytes)"));
    }

    /* Calling deposit functions requires that sender has approved this contract. */
    function deposit(
        address token,
        uint256 tokenIdOrAmount
    ) external {
        isNotContractBlacklisted(token, _msgSender());
        isNotBlacklisted(_msgSender());
        bridgeIsOpen();

        address sender = _msgSender();
        
        try IERC165(token).supportsInterface(_ERC721_INTERFACE) returns (bool isERC721) {
            if (isERC721) {
                //we use IERC721(token).transferFrom instead of IERC721(token).safeTransferFrom
                //for backward compatibility with traditional ERC721 NFTs
                //this call will not invoke onERC721Received in this contract so 
                //we have to emit the event here
                IERC721(token).transferFrom(sender, address(this), tokenIdOrAmount);
                emit Deposit(token, sender, _tokenDepositNonces[token][sender], tokenIdOrAmount, 1);
                _tokenDepositNonces[token][sender] = _tokenDepositNonces[token][sender].add(1);
            
            } else if(IERC165(token).supportsInterface(_ERC1155_INTERFACE)) {
                revert(string(abi.encodePacked("Bridge: Use correct ERC1155 deposit")));

            } else if(IERC165(token).supportsInterface(_ERC20_INTERFACE)) {
                //An ERC20 with supports interface method
                _erc20Desposit(sender, token, tokenIdOrAmount);
            } else {
                //revert("Unsupported Token Interface");
                //Assume ERC20, let the error be thrown from that side if its not
                _erc20Desposit(sender, token, tokenIdOrAmount);
            }
        } catch {
            //This is ERC20
            _erc20Desposit(sender, token, tokenIdOrAmount);
            
        }
    }

    function withdraw(
        address token,
        uint256 tokenIdOrAmount,
        address recipient
    ) external {
      canControlBridge();

      string memory invalidMsg = "No ERC20 Withdraw";

      try IERC165(token).supportsInterface(_ERC721_INTERFACE) returns (bool isERC721) {
          if (isERC721) {
            IERC721(token).safeTransferFrom(address(this), recipient, tokenIdOrAmount);

          } else if(IERC165(token).supportsInterface(_ERC1155_INTERFACE)) {
            revert(string(abi.encodePacked("Bridge: Use correct ERC1155 withdraw")));

          } else {
            revert(invalidMsg);
          }
      } catch {
        //This is maybe an ERC20
        revert(invalidMsg);
      }
      
      emit Withdraw(token, tokenIdOrAmount, tokenIdOrAmount, recipient);
    }

    function deposit(
        address token,
        uint256 tokenId,
        uint256 tokenValue
    ) external {
        _deposit(token, tokenId, tokenValue, "");
    }

    function withdraw(
        address token,
        uint256 tokenId,
        uint256 tokenValue,
        address recipient
    ) external {
      _withdraw(token, tokenId, tokenValue, recipient, "");
    }

    function deposit(
        address token,
        uint256 tokenId,
        uint256 tokenValue,
        bytes calldata data
    ) external {
        _deposit(token, tokenId, tokenValue, data);
    }

    function withdraw(
        address token,
        uint256 tokenId,
        uint256 tokenValue,
        address recipient,
        bytes calldata data
    ) external {
      _withdraw(token, tokenId, tokenValue, recipient, data);
    }

    function _deposit(
        address token,
        uint256 tokenId,
        uint256 tokenValue,
        bytes memory data
    ) private {

        isNotContractBlacklisted(token, _msgSender());
        isNotBlacklisted(_msgSender());
        isERC1155(token);
        bridgeIsOpen();

        IERC1155(token).safeTransferFrom(_msgSender(), address(this), tokenId, tokenValue, data);

        //IERC1155(token).safeTransferFrom should automatically invoke onERC1155Received on this contract
        //so there is no need to emit the deposit event here
        //emit Deposit(token, _msgSender(), tokenId, tokenValue);
    }

    function _withdraw(
        address token,
        uint256 tokenId,
        uint256 tokenValue,
        address recipient,
        bytes memory data) private {
         isERC1155(token);
        canControlBridge();

        IERC1155(token).safeTransferFrom(address(this), recipient, tokenId, tokenValue, data);
        emit Withdraw(token, tokenId, tokenValue, recipient);
    }

    function _msgSender()
        internal
        view
        override(ContextUpgradeable, EIP712MetaTransactionUpgradeable)
        returns (address)
    {
        return ContextMixin.msgSender();
    }

    function _erc20Desposit(address sender, address token, uint256 tokenIdOrAmount) internal virtual {
        uint256 allowance = IERC20(token).allowance(sender, address(this));
        require(allowance >= tokenIdOrAmount, "Bridge: Not Approved On User Token");

        ERC20Burnable(token).burnFrom(sender, tokenIdOrAmount);

        emit ERC20Deposit(
            token, 
            sender, 
            _tokenDepositNonces[token][sender], 
            tokenIdOrAmount, 
            IERC20(token).balanceOf(sender)
        );

        emit Deposit(token, sender, _tokenDepositNonces[token][sender], 0, tokenIdOrAmount);

        _tokenDepositNonces[token][sender] = _tokenDepositNonces[token][sender].add(1);
    }

    function withdrawEther() external {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Not Admin");
        payable(_msgSender()).transfer(address(this).balance);
    }

    function allowContract(address contractAddress, address partnerWallet) external {
        isContract(contractAddress);
        canControlBridge();

        _whiteListedTokens[contractAddress] = partnerWallet;
        emit PartnerEvent(contractAddress, block.timestamp, partnerWallet, "AddedOrUpdated");
    }

    function blockContract(address contractAddress) external {
        isContract(contractAddress);
        canControlBridge();

        address partnerWallet = _whiteListedTokens[contractAddress];
        delete _whiteListedTokens[contractAddress];
        emit PartnerEvent(contractAddress, block.timestamp, partnerWallet, "Removed");
    }

    /* for ERC721 */
    function partnerWithdrawToken(address contractAddress, uint256 tokenId) external {
        isContractAllowed(contractAddress);
        isNotContractBlacklisted(contractAddress, _msgSender());
        isNotBlacklisted(_msgSender());

        address partnerWallet = _whiteListedTokens[contractAddress];
        require(partnerWallet == _msgSender(), "Unauthorized Withdrawal");

        IERC721(contractAddress).safeTransferFrom(address(this), _msgSender(), tokenId);
    }

    /* for ERC1155 */
    function partnerWithdrawToken(address contractAddress, uint256 token, uint256 amount) external {
        isContractAllowed(contractAddress);
        isNotContractBlacklisted(contractAddress, _msgSender());
        isNotBlacklisted(_msgSender());

        address partnerWallet = _whiteListedTokens[contractAddress];
        require(partnerWallet == _msgSender(), "Unauthorized Withdrawal");

        IERC1155(contractAddress).safeTransferFrom(address(this), _msgSender(), token, amount, "");
    }
}
