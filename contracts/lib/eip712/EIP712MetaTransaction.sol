// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { SafeMath } from "@openzeppelin/contracts/math/SafeMath.sol";
import { EIP712Base } from "./EIP712Base.sol";
import { ContextMixin } from "../common/ContextMixin.sol";

contract EIP712MetaTransaction is EIP712Base, ContextMixin {
    using SafeMath for uint256;
    
    bytes32 private constant META_TRANSACTION_TYPEHASH = keccak256(
        bytes(
            "MetaTransaction(uint256 nonce,address from,bytes functionSignature)"
        )
    );
    event MetaTransactionExecuted(
        address userAddress,
        address payable relayerAddress,
        bytes functionSignature
    );

    //mapping(address => uint256) nonces;
    
    // improved signer to sender nonce mapping
    mapping(address => mapping(address => uint256)) nonces;

    /*
     * Meta transaction structure.
     * No point of including value field here as if user is doing value transfer then he has the funds to pay for gas
     * He should call the desired function directly in that case.
     */
    struct MetaTransaction {
        uint256 nonce;
        address from;
        bytes functionSignature;
    }

    function executeMetaTransaction(
        address signerAddress,
        bytes memory functionSignature,
        bytes32 sigR,
        bytes32 sigS,
        uint8 sigV
    ) public payable returns (bytes memory) {
        address payable sender = payable(_msgSender());

        MetaTransaction memory metaTx = MetaTransaction({
            //nonce: nonces[userAddress], //this will be the nonce for the user that sends and pays this transaction
            nonce: nonces[signerAddress][sender], //this will be the nonce for the user that relays and pays this transaction
            from: signerAddress, //signer stays the same, whomever is authorized to do the transaction
            functionSignature: functionSignature
        });

        require(
            verify(signerAddress, metaTx, sigR, sigS, sigV),
            "Signer and signature do not match"
        );

        // increase nonce for user (to avoid re-use)
        // nonces[userAddress] = nonces[userAddress].add(1);
        nonces[signerAddress][sender] = nonces[signerAddress][sender].add(1);

        emit MetaTransactionExecuted(
            signerAddress,
            sender,
            functionSignature
        );

        // Append userAddress and relayer address at the end to extract it from calling context
        (bool success, bytes memory returnData) = address(this).call(
            abi.encodePacked(functionSignature, signerAddress)
        );
        require(success, "Function call not successful");

        return returnData;
    }

    function hashMetaTransaction(MetaTransaction memory metaTx)
        internal
        pure
        returns (bytes32)
    {
        return
            keccak256(
                abi.encode(
                    META_TRANSACTION_TYPEHASH,
                    metaTx.nonce,
                    metaTx.from,
                    keccak256(metaTx.functionSignature)
                )
            );
    }

    function getNonce(address sender) public view returns (uint256 nonce) {
        address signer = _msgSender();
        nonce = nonces[signer][sender];
    }

    function verify(
        address signer,
        MetaTransaction memory metaTx,
        bytes32 sigR,
        bytes32 sigS,
        uint8 sigV
    ) internal view returns (bool) {
        require(signer != address(0), "NativeMetaTransaction: INVALID_SIGNER");
        return
            signer ==
            ecrecover(
                toTypedMessageHash(hashMetaTransaction(metaTx)),
                sigV,
                sigR,
                sigS
            );
    }

    // This is to support Native meta transactions
    // never use msg.sender directly, use _msgSender() instead
    function _msgSender()
        internal
        view
        virtual
        returns (address sender)
    {
        return ContextMixin.msgSender();
    }
}
