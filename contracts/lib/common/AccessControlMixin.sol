// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { AccessControl } from "@openzeppelin/contracts/access/AccessControl.sol";

abstract contract AccessControlMixin is AccessControl {
    string private _revertMsg;

    function _setupContractId(string memory contractId) internal {
        _revertMsg = string(
            abi.encodePacked(contractId, ": INSUFFICIENT_PERMISSIONS")
        );
    }

    modifier withRole(bytes32 role) {
        require(hasRole(role, _msgSender()), _revertMsg);
        _;
    }
}
