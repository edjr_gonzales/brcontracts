// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { ERC20PresetMinterPauser } from "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol";

abstract contract PauserMixin is ERC20PresetMinterPauser {
    function setPauser(address pauserAcc, bool isGrant) public {
      if (isGrant){
        grantRole(PAUSER_ROLE, pauserAcc);
      } else {
        revokeRole(PAUSER_ROLE, pauserAcc);
      }
    }

    function isPauser(address pauserAcc) public view returns (bool) {
      return hasRole(PAUSER_ROLE, pauserAcc);
    }
}
