// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { ERC20PresetMinterPauser } from "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol";

abstract contract MinterMixin is ERC20PresetMinterPauser {
    function setMinter(address minterAcc, bool isGrant) public {
      if (isGrant){
        grantRole(MINTER_ROLE, minterAcc);
      } else {
        revokeRole(MINTER_ROLE, minterAcc);
      }
    }

    function isMinter(address minterAcc) public view returns (bool) {
      return hasRole(MINTER_ROLE, minterAcc);
    }
}
