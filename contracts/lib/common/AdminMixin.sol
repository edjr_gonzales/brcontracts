// SPDX-License-Identifier: OSL-3.0

pragma solidity ^0.8.0;

import { AccessControl } from "@openzeppelin/contracts/access/AccessControl.sol";

abstract contract AdminMixin is AccessControl {
    function setAdmin(address adminAcc, bool isGrant) public {
      if (isGrant){
        grantRole(DEFAULT_ADMIN_ROLE, adminAcc);
      } else {
        revokeRole(DEFAULT_ADMIN_ROLE, adminAcc);
      }
    }

    function isAdmin(address adminAcc) public view returns (bool) {
      return hasRole(DEFAULT_ADMIN_ROLE, adminAcc);
    }
}
