/**
 * Use this file to configure your truffle project. It's seeded with some
 * common settings for different networks and features like migrations,
 * compilation and testing. Uncomment the ones you need or modify
 * them to suit your project as necessary.
 *
 * More information about configuration can be found at:
 *
 * truffleframework.com/docs/advanced/configuration
 *
 * To deploy via Infura you'll need a wallet provider (like truffle-hdwallet-provider)
 * to sign your transactions before they're sent to a remote public node. Infura API
 * keys are available for free at: infura.io/register
 *
 * You'll also need a mnemonic - the twelve word phrase the wallet uses to generate
 * public/private key pairs. If you're publishing your code to GitHub make sure you load this
 * phrase from a file you've .gitignored so it doesn't accidentally become public.
 *
 */

const path = require('path');
const fs = require('fs');

require('dotenv').config();

const HDWalletProvider = require('@truffle/hdwallet-provider');
const INFURA_KEY = process.env.INFURA_KEY;
const MNEMONIC = process.env.MNEMONIC;
const LOOM_KEY = process.env.LOOM_KEY;

const { etherscanApiKey } = require('./secrets.json');

const solc_settings = { 
  evmVersion: "istanbul",
  optimizer: {
    enabled: true,
    runs: 200
  }
};

console.log("solc_settings ", solc_settings)

module.exports = {
  /**
   * Networks define how you connect to your ethereum client and let you set the
   * defaults web3 uses to send transactions. If you don't specify one truffle
   * will spin up a development blockchain for you on port 9545 when you
   * run `develop` or `test`. You can ask a truffle command to use a specific
   * network from the command line, e.g
   *
   * $ truffle test --network <network-name>
   */

  contracts_build_directory: process.env.NODE_ENV === "test" ? path.resolve(process.cwd(), "test_build") : path.resolve(process.cwd(), "build/contracts"),

  networks: {
    // Useful for testing. The `development` name is special - truffle uses it by default
    // if it's defined here and no other network is specified at the command line.
    // You should run a client (like ganache-cli, geth or parity) in a separate terminal
    // tab if you use this network and you must also set the `host`, `port` and `network_id`
    // options below to some value.

  // dev nets
  dev_matic: {
    host: "host.docker.internal",     // Localhost (default: none)
    port: 8545,            // Standard Ethereum port (default: none)
    network_id: "721",       // Any network (default: none)
  },

	dev_mainnet: {
    provider: function() {
      return new HDWalletProvider(
        MNEMONIC,
        "http://host.docker.internal:9545",
        0, 20, false
      );
    },
    network_id: "821",       // Any network (default: none)
  },

  development: {
    /* provider: function() {
      return new HDWalletProvider(
        MNEMONIC,
        "http://localhost:7357",
        0, 20, false
      );
    }, */
    host: "localhost",
    port: "7357",
    /* provider: function() {
      return new HDWalletProvider({
        mnemonic: MNEMONIC,
        providerOrUrl: "http://localhost:7357",
        shareNonce: false,
      });
    }, */
    network_id: "7357",    // TEST Net ID
  },

  //test nets, dont remove
  test_matic: {
    host: "test-matic",
    port: 8545,
    network_id: "*",
  },

	test_mainnet: {
    provider: function() {
      return new HDWalletProvider(
        MNEMONIC,
        "http://test-mainnet:8545",
        0, 20, false
      );
    },
    network_id: "*",
  },

  test: {
    provider: function() {
      return new HDWalletProvider(
        MNEMONIC,
        "http://localhost:7357",
        0, 20, false
      );
    },
    network_id: "7357",    // TEST Net ID
  },

  mainnet: {
    network_id: 1,
    provider: function() {
      return new HDWalletProvider(
        MNEMONIC,
        "https://mainnet.infura.io/v3/" + INFURA_KEY
      );
    },
    gas: 6712388,
    gasPrice: 16000000000
  },

    // Another network with more advanced options...
    // advanced: {
      // port: 8777,             // Custom port
      // network_id: 1342,       // Custom network
      // gas: 8500000,           // Gas sent with each transaction (default: ~6700000)
      // gasPrice: 20000000000,  // 20 gwei (in wei) (default: 100 gwei)
      // from: <address>,        // Account to send txs from (default: accounts[0])
      // websockets: true        // Enable EventEmitter interface for web3 (default: false)
    // },

    // Useful for deploying to a public network.
    // NB: It's important to wrap the provider as a function.
    // ropsten: {
      // provider: () => new HDWalletProvider(mnemonic, `https://ropsten.infura.io/${infuraKey}`),
      // network_id: 3,       // Ropsten's id
      // gas: 5500000,        // Ropsten has a lower block limit than mainnet
      // confirmations: 2,    // # of confs to wait between deployments. (default: 0)
      // timeoutBlocks: 200,  // # of blocks before a deployment times out  (minimum/default: 50)
      // skipDryRun: true     // Skip dry run before migrations? (default: false for public nets )
    // },

    rinkeby: {
      provider: function() {
        return new HDWalletProvider(
          MNEMONIC,
          "https://rinkeby.infura.io/v3/" + INFURA_KEY,
          0,
          2
        );
      },
      skipDryRun: true,
      network_id: 4
    },

    ropsten: {
      provider: function() {
        return new HDWalletProvider(
          MNEMONIC,
          "https://ropsten.infura.io/v3/" + INFURA_KEY
        );
      },
      skipDryRun: true,
      network_id: 3
    },

    matic_testnet: {
      provider: function() {
        return new HDWalletProvider(
          MNEMONIC,
          "https://testnet2.matic.network",
          0,
          4
        );
      },
      gasPrice: 0,
      skipDryRun: true,
      network_id: 8995
    },

    matic_mumbai: {
      //provider: () => new HDWalletProvider(MNEMONIC, `https://rpc-mumbai.maticvigil.com`, 0, 2),
      provider: () => new HDWalletProvider(MNEMONIC, `https://rpc-mumbai.maticvigil.com/v1/adede787e5a45f92773bcdd1200a4e30c71dfaa3`, 0, 2),
      network_id: 80001,
      confirmations: 2,
      timeoutBlocks: 200,
      skipDryRun: true,
      networkCheckTimeout: 15000,
      chainId: 80001
    },

    matic_alpha: {
      provider: function() {
        return new HDWalletProvider(
          MNEMONIC,
          "https://alpha.ethereum.matic.network",
          0,
          3
        );
      },
      skipDryRun: true,
      network_id: 4626
    },

    matic_mainnet: {
      provider: function() {
        return new HDWalletProvider(
          MNEMONIC,
          "https://rpc-mainnet.maticvigil.com/"
//          "https://rpc-mainnet.matic.network"
        );
      },
      skipDryRun: true,
      network_id: 137,
      confirmations: 2,
      timeoutBlocks: 200,
      networkCheckTimeout: 30000
    },

    // Useful for private networks
    // private: {
      // provider: () => new HDWalletProvider(mnemonic, `https://network.io`),
      // network_id: 2111,   // This network is yours, in the cloud.
      // production: true    // Treats this network as if it was a public net. (default: false)
    // }
  },

  // Set default mocha options here, use special reporters etc.
  mocha: {
    // timeout: 100000
  },

  // Configure your compilers
  compilers: {
    solc: {
      version: "0.8.6",    // Fetch exact version from solc-bin (default: truffle's version)
	    settings: solc_settings,
      // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
      // settings: {          // See the solidity docs for advice about optimization and evmVersion
      //  optimizer: {
      //    enabled: false,
      //    runs: 200
      //  },
      //  evmVersion: "byzantium"
      // }
    }
  },

  plugins: [
    'truffle-plugin-verify'
  ],
  api_keys: {
    etherscan: etherscanApiKey
  }
}
