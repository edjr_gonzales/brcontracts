const { ethers, upgrades } = require("hardhat");

async function main() {
  const ScrapV2 = await ethers.getContractFactory("ScrapV2");
  const scrap = await upgrades.deployProxy(ScrapV2, [ "0x9196eCFeEdF3A7F135E36e712a9d52079e76E6ef", "Battle Racers Scrap", "$SCRAP", "2" ], { initializer: "initializeScrapV2" });

  await scrap.deployed();
  console.log("ScrapV2 deployed to:", scrap.address);

  await scrap.grantRole(web3.utils.sha3("MINTER_ROLE"), "0x1331B779A3D4Bd3f7E605e8c8A7D4Edcf692Fd6b");  
}

main();