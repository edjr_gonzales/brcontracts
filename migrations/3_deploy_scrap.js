const Scrap = artifacts.require("Scrap");

const deployScrap = async (deployer) => {
	await deployer.deploy(Scrap, "BR Scrap", "BRSCRAP", "1");
};

module.exports = async (deployer, network) => {
	console.log(`3: Deploying to ${network}`);
	
	switch(network){
		case "test_matic":
		case "dev_matic":
		case "matic_mainnet":
		case "matic_testnet":
		case "matic_mumbai":
			await deployScrap(deployer);
			break;
		
		case "test_mainnet":
		case "dev_mainnet":
		case "mainnet":
		case "rinkeby":
		case "ropsten":
			await deployScrap(deployer);
			break;

		//cases for simple testing
		case "test":
		case "development":
		default:
			await deployScrap(deployer);
			break;
	}
};
