
const { deployProxy } = require('@openzeppelin/truffle-upgrades');

//Note: This is an upgradable contract
const PlaycheckGateway = artifacts.require("PlaycheckGateway");

const deployChainPortal = async (deployer) => {
	const instance = await deployProxy(PlaycheckGateway, ["Playcheck Gateway", "PLAYCHKGW", "1"], { deployer });
  console.log('Deployed PlaycheckGateway', instance.address);
};

module.exports = async (deployer, network) => {
	console.log(`4: Deploying to ${network}`);
	
	switch(network){
		case "test_matic":
		case "dev_matic":
		case "matic_mainnet":
		case "matic_testnet":
		case "matic_mumbai":
			await deployChainPortal(deployer);
			break;
		
		case "test_mainnet":
		case "dev_mainnet":
		case "mainnet":
		case "rinkeby":
		case "ropsten":
			await deployChainPortal(deployer);
			break;

		//cases for simple testing
		case "test":
		case "development":
		default:
			await deployChainPortal(deployer);
			break;
	}
};
