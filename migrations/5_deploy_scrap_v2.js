
const { deployProxy } = require('@openzeppelin/truffle-upgrades');

//Note: This is an upgradable contract
const Scrap = artifacts.require("Scrap");
const ScrapV2 = artifacts.require("ScrapV2");

const deployUpgradeableScrap = async (deployer) => {
	//test/development deploy
  const original = await Scrap.deployed();
	const instance = await deployProxy(ScrapV2, [original.address, "Battle Racers Scrap", "$SCRAP", "2"], { deployer, initializer: "initializeScrapV2" });

	//mumbai staging deploy
	//const instance = await deployProxy(ScrapV2, ["0x9196eCFeEdF3A7F135E36e712a9d52079e76E6ef", "Battle Racers Scrap", "$SCRAP", "2"], { deployer, initializer: "initializeScrapV2" });
	
  console.log('Deployed ScrapV2', instance.address);
};

module.exports = async (deployer, network) => {
	console.log(`5: Deploying to ${network}`);
	
	switch(network){
		case "test_matic":
		case "dev_matic":
		case "matic_mainnet":
		case "matic_testnet":
		case "matic_mumbai":
			await deployUpgradeableScrap(deployer);
			break;
		
		case "test_mainnet":
		case "dev_mainnet":
		case "mainnet":
		case "rinkeby":
		case "ropsten":
			await deployUpgradeableScrap(deployer);
			break;

		//cases for simple testing
		case "test":
		case "development":
		default:
			await deployUpgradeableScrap(deployer);
			break;
	}
};
