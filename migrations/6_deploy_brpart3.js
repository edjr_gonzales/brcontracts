const BRPart = artifacts.require("BRPart");
const BRNftBridge = artifacts.require("BRNftBridge");

const maticDeploy = async (deployer) => {
	await deployer.deploy(BRPart, "Battle Racers Part", "BRPART", "3");
	// await deployer.deploy(BRNftBridge, "Battle Racers Matic NFT Bridge", "MATICBRNFTBRIDGE", "1");
};

const ethereumDeploy = async (deployer) => {
	// await deployer.deploy(BRNftBridge, "Battle Racers Ethereum NFT Bridge", "ETHBRNFTBRIDGE", "1");
};

const approveBridge = async () => {
	const part = await BRPart.deployed();
	const bridge = await BRNftBridge.deployed();

	part.setApprovalForAll(bridge.address, true);
};

module.exports = async (deployer, network) => {
	console.log(`6: Deploying to ${network}`);
	
	switch(network){
		case "test_matic":
		case "dev_matic":
		case "matic_mainnet":
		case "matic_testnet":
		case "matic_mumbai":
			await maticDeploy(deployer);
			break;
		
		case "test_mainnet":
		case "dev_mainnet":
		case "mainnet":
		case "rinkeby":
		case "ropsten":
			await ethereumDeploy(deployer);
			break;

		//cases for simple testing
		case "test":
		case "development":
		default:
			await maticDeploy(deployer);
			break;
	}
};
