const { expect } = require('chai');
const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

const testEIP712(name, contract, opts) => {
  return {
    'can perform meta-transaction call': () => {
      const data = await web3.eth.abi.encodeFunctionCall({
        name: 'transferFrom', 
        type: 'function', 
        "inputs": [
          {
            "name": "token",
            "type": "address"
          },
          {
            "name": "tokenId",
            "type": "uint256"
          }
        ]
      }, [ maticPartAddress, metadata.id ]);
    }
  };
  
}