const getSignatureParameters = (signature) => {
  if (!web3.utils.isHexStrict(signature)) {
    throw new Error(
      'Given value "'.concat(signature, '" is not a valid hex string.')
    );
  }
  
  var r = signature.slice(0, 66);
  var s = "0x".concat(signature.slice(66, 130));
  var v = web3.utils.toDecimal(`0x${signature.slice(130, 132)}`)  + 27; 
  
  v = "0x".concat(signature.slice(130, 132));
  v = web3.utils.hexToNumber(v);
  if (![27, 28].includes(v)) v += 27;
  
  return {
    r: r,
    s: s,
    v: v,
  };
};

module.exports = {
  getSignatureParameters
}