const { expect } = require('chai');
const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

const BN1 = new BN('1');
const BN10 = new BN('10');

const getMintableTests = () => {
  return {
    'can mint tokens': {
      fn: async (contract, [ minter ]) => {
        //console.log("testing: can mint tokens...");
        //console.log("minter", minter);
  
        const recipientBal = await contract.balanceOf(minter);
        await contract.mint(minter, 1, { from: minter });
        const recipientNewBal = await contract.balanceOf(minter);
    
        return recipientNewBal.eq(recipientBal.add(BN1));
      },
      expect: true
    }
  };
}

const getBurnableTests = () => {
  return {
    'can mint tokens': {
      fn: async (contract, [ minter ]) => {
        //console.log("testing: can burn tokens...");
        //console.log("minter", minter);
  
        await contract.mint(minter, 1, { from: minter });
        const recipientBal = await contract.balanceOf(minter);

        await contract.burn(1, { from: minter });
        const recipientNewBal = await contract.balanceOf(minter);
    
        return recipientNewBal.eq(recipientBal.sub(BN1));
      },
      expect: true
    }
  };
}

const getPausableTests = () => {
  return {
    'can pause token': {
      fn: async (contract, [ admin ]) => {
        await contract.pause({ from: admin });
        return await contract.paused();
      },
      expect: true
    },
    'can unpause token': {
      fn: async (contract, [ admin ]) => {
        await contract.unpause({ from: admin });
        return await contract.paused();
      },
      expect: false
    }
  };
}

const getAccessControlTests = () => {
  return {
    'can add admin using owner account': {
      fn: async (contract, [ owner, user1 ]) => {
        await contract.setAdmin(user1, true, { from: owner });
        return await contract.isAdmin(user1);
      },
      expect: true
    },
    'can remove admin using owner account': {
      fn: async (contract, [ owner, user1 ]) => {
        await contract.setAdmin(user1, false, { from: owner });
        return await contract.isAdmin(user1);
      },
      expect: false
    }
  };
}

const getTests = () => {
  return {
    'can transfer token': {
      fn: async (contract, [ admin, user1 ]) => {
        await contract.mint(admin, 2, { from: admin });

        const adminBal = await contract.balanceOf(admin);
        const user1Bal = await contract.balanceOf(user1);

        await contract.transfer(user1, 1, { from: admin });
        return (await contract.balanceOf(admin)).eq(adminBal.sub(BN1)) && 
          (await contract.balanceOf(user1)).eq(user1Bal.add(BN1));
      },
      expect: true
    },
    'can grant allowance': {
      fn: async (contract, [ admin, spender ]) => {
        const result = await contract.approve(spender, 10, { from: admin });
        const allowance = await contract.allowance(admin, spender);

        //console.log("\t\tallowance", allowance, result);
        return (allowance).eq(BN10);
      },
      expect: true
    }
  };
}

module.exports = {
  getTests,
  getMintableTests,
  getBurnableTests,
  getPausableTests,
  getAccessControlTests
};