require('@nomiclabs/hardhat-ethers');
require('@openzeppelin/hardhat-upgrades');
require("@nomiclabs/hardhat-etherscan");

require('dotenv').config();

const { etherscanApiKey } = require('./secrets.json');

/**
 * @type import('hardhat/config').HardhatUserConfig
 */

const INFURA_KEY = process.env.INFURA_KEY;
const MNEMONIC = process.env.MNEMONIC;

module.exports = {
  defaultNetwork: "hardhat",
  networks: {
    hardhat: {
      hardfork: 'istanbul'
    },
    rinkeby: {
      url: `https://rinkeby.infura.io/v3/${INFURA_KEY}`,
      chainId: 4,
      accounts: {
        mnemonic: MNEMONIC,
        count: 4,
        initialIndex: 0,
        path: "m/44'/60'/0'/0",
      }
    },
    mumbai: {
      url: `https://rpc-mumbai.maticvigil.com/v1/adede787e5a45f92773bcdd1200a4e30c71dfaa3`,
      chainId: 80001,
      gas: 21000000,
      gasPrice: "auto",
      gasMultiplier: 1,
      accounts: {
        mnemonic: MNEMONIC,
        count: 4,
        initialIndex: 0,
        path: "m/44'/60'/0'/0",
      }
    },
    matic: {
      url: `https://rpc-mainnet.maticvigil.com/v1/adede787e5a45f92773bcdd1200a4e30c71dfaa3`,
      chainId: 137,
      gas: 21000000,
      gasPrice: "auto",
      gasMultiplier: 1,
      accounts: {
        mnemonic: MNEMONIC,
        count: 4,
        initialIndex: 0,
        path: "m/44'/60'/0'/0",
      }
    },
  },
  solidity: {
    version: "0.8.6",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      },
      evmVersion: "istanbul"
    }
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },
  mocha: {
    timeout: 20000
  },
  etherscan: {
    // Your API key for Etherscan
    // Obtain one at https://etherscan.io/
    apiKey: etherscanApiKey
  }
}