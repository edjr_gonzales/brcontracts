FROM node:12.19.0-alpine

LABEL maintainer egonzales@altitude-games.com

RUN apk update \
    && apk add --virtual build-dependencies libusb-dev eudev-dev linux-headers \
        build-base \
        gcc \
        wget \
        git \
        python3 \
    && apk add \
        bash

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY package*.json ./
COPY docker-ecosystem.sh ./
COPY . ./

RUN chmod +x ./docker-ecosystem.sh

RUN npm install --save

CMD [ "./docker-ecosystem.sh" ]
