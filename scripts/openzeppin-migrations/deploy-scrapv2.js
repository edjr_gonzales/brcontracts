const { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');

const Scrap = artifacts.require("Scrap");
const ScrapV2 = artifacts.require('ScrapV2');

const deployScrapV2 = async (deployer, scrap) => {
  console.log("Wrapping scrap instance on " + scrap.address);

  //deploy the upgradable ScrapV2 contract
  const instance = await deployProxy(ScrapV2, [scrap.name(), scrap.symbol(), "2", scrap.address], { deployer });
  console.log('Deployed', instance.address);
};

module.exports = async function (deployer, network) {
  const instance = await Scrap.deployed();
  //const upgraded = await upgradeProxy(instance.address, ScrapV2, { deployer });

  console.log(`Deploying to ${network}`);
	
	switch(network){
		case "test_matic":
		case "dev_matic":
		case "matic_mainnet":
		case "matic_testnet":
		case "matic_mumbai":
			await deployScrapV2(deployer, instance);
			break;
		
		case "test_mainnet":
		case "dev_mainnet":
		case "mainnet":
		case "rinkeby":
		case "ropsten":
			await deployScrapV2(deployer, instance);
			break;

		//cases for simple testing
		case "test":
		case "development":
		default:
			await deployScrapV2(deployer, instance);
			break;
	}
}