**Istanbul based EVM Contracts**

This repository was made to factor out istanbul evm contract types away from default EVM used on [battlecars-monorepo](https://bitbucket.org/altitude-games/battlecars-monorepo).

---

## Steps to start using docker based development.

Your workstation computer must have docker installed. Make sure that it can support docker-compose configuration version 3.7 and higher.

1. Start docker VM by `docker-compose up` command. *Docker will build the image for the first time but tdoing this on the future will just directly boot up the existing image.*
2. Once docker started, you may open a terminal to it by `docker exec -it battlecars-istanbul-contracts_contracts_1 /bin/bash`.
3. Press CTRL + D to exit docker terminal.

Press CTRL + C on docker-compose window to terminate docker.

---

## Adjust Configuration

Add or remove additional environment configuration under truffle-config.js.

---

## Compiling Contracts

Use `truffle compile` to compile your contracts and check for errors.

Use `truffle migrate --network truffle_network_config_here` to deploy the contracts to chain.
